Declare Credentials in JenkinsFile

`
pipeline {

    agent any
    environment {
        NEW_VERSION = '1.3.0'
        SERVER_CREDENTIALS = credentials('')
    }
    stages{
        stage("build") {
            steps {
                echo 'building the application'
                echo "building version ${NEW_VERSION}"
            }
        }
`

`
  stage("deploy") {
            steps {
                echo 'deploying application'
                withCredentials([
                    // 'server-credentials' is the name of credentials in jenkins global creditials
                    usernamePassword(credentials: 'server-credentials', usernameVariable: USER, passwordVariable: PWD)          

                ]) {
                    sh "some script ${USE} ${PWD}"
                }
            }

        }
`